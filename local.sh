kill -9 $(lsof -i:5801 -t) &

NODE_ENV=local npm run --prefix server debug &
NODE_ENV=local npm run start --prefix client
