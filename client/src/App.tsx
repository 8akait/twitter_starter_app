import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TwitterOAuth from './screens/Twitter/Twitter.oAuth';
import TweetScreen from './screens/Tweets/Tweet.screen';
import { Route, Switch } from 'react-router-dom';
import Auth from './screens/Auth/Auth.redirect';
import  { Redirect } from 'react-router-dom';
export interface Props {}

interface TwitterUserType {
  id: string;
  screen_name: string;
  location?: string;
}

interface EntitiesType {
  hashtags: string[];
  user_mentions: TwitterUserType[];
}
export interface TweetType {
  created_at: string;
  id: string;
  text: string;
  source: string;
  user: TwitterUserType;
  entities: EntitiesType;
}

export interface TwitterDataType {
  id: string;
  token: string;
  tweets: TweetType[];
}

export interface userDataType {
  name: string;
  email: string;
  twitter: TwitterDataType;
}
interface State {
  user: userDataType | undefined
}

class App extends Component<Props, State>{
  constructor(props: Props){
    super(props);
    this.state = {
      user: undefined
    }
    this.setUserDataAndRedirect = this.setUserDataAndRedirect.bind(this);
  }
  setUserDataAndRedirect(userData: userDataType){
    this.setState({user: userData})
  }

  render() {
    return (
      <div className="App">
        <Switch>     
            <Route 
              exact path="/auth/redirect"
              render={(props: any) => {
                return <Auth setUserData={this.setUserDataAndRedirect} {...props} userData={this.state.user}/>
              }} 
            />
            <PrivateRoute path='/user' Comp={TweetScreen} userData={this.state.user} />
            <Route path="/" component={TwitterOAuth} />
        </Switch>
      </div>
    );
  }
}
const PrivateRoute = ({path, Comp, userData}: any) => {
  if(userData)
    return <Route  
        path={path}
        render={routeProps => <Comp userData={userData} {...routeProps}/>}
      />
  else return <Redirect to={{pathname: '/' }} />;
}
export default App;
