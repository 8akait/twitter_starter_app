import axios from 'axios';

const serverAxios =(() => {
  const env = process.env.NODE_ENV || 'development';
  if(env === 'development'){
    return axios.create({
      baseURL: 'http://localhost:4000',
      timeout: 100000,
      headers: {'X-Custom-Header': 'foobar'}
    });
  }
  return axios.create({
    baseURL: 'http://localhost:3050/api',
    timeout: 100000,
    headers: {'X-Custom-Header': 'foobar'}
  });
  
})() 

export {
    serverAxios
};