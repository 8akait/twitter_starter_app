import React from 'react';
import queryString from 'query-string';
import { isArray } from 'util';
import  { Redirect } from 'react-router-dom';

const auth = (props: any) => {
    const query =  queryString.parse(props.location.search);
    let userData = undefined;
    if(query && query.user && !isArray(query.user))
        userData = JSON.parse(query.user);
    props.setUserData(userData);
    if(props.userData){
        return <Redirect to={{
            pathname: '/user'
        }} />
    } else {
        return <></>;
    }
   
}
export default React.memo(auth);