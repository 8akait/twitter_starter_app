import React, { Component} from 'react';
import './Tweet.screen.css';
import {serverAxios} from '../../axios/server.axios';
import {userDataType, TweetType} from '../../App';
import TweetCard from './Tweet.card';

export interface Props {
    userData: userDataType
}
interface State {
  tweets: TweetType[];
  hashTagSearchStr: string;
  filterByLocationStr: string;
}

class TweetScreen extends Component<Props, State>  {
  constructor(props: Props){
    super(props);
    this.state = {
      tweets: this.props.userData.twitter.tweets,
      hashTagSearchStr: '',
      filterByLocationStr: ''
    }
    this.filterDataByhashTag = this.filterDataByhashTag.bind(this);
    this.filterDataByLOcation = this.filterDataByLOcation.bind(this);
  }
  // static getDerivedStateFromProps(nextProps: Props, prevState: State){
  //   const newTweets = nextProps.userData.twitter.tweets;
  //   if (JSON.stringify(prevState.tweets) !== JSON.stringify(newTweets)) {
  //       return {
  //         tweets: newTweets
  //       };
  //   }
  //   return null;
  // }

  filterDataByhashTag(event: any) {
    const str = event.target.value.toLowerCase();
    const {tweets} = this.props.userData.twitter;
    let filteredTweets = tweets;

    if(str){
      filteredTweets = tweets.filter((tweet) => {
        const {hashtags} = tweet.entities;
        let isExist = false;
        hashtags.map((tag) => {
          if(tag.toLowerCase().indexOf(str) >= 0){
            isExist = true;
          }
        })
        return isExist;
      })
    }
    
    this.setState({
      tweets: filteredTweets,
      hashTagSearchStr: str,
      filterByLocationStr: ''
    })
  }
  filterDataByLOcation(event: any) {
    const str = event.target.value.toLowerCase();
    const {tweets} = this.props.userData.twitter;
    let filteredTweets = tweets;
    if(str){
      filteredTweets = tweets.filter((tweet) => {
        const {location} = tweet.user;
        let isExist = false;
          if(location && location.toLowerCase().indexOf(str) >= 0){
            isExist = true;
          }
        return isExist;
      })
    }
    this.setState({
      tweets: filteredTweets,
      hashTagSearchStr: '',
      filterByLocationStr: str
    })
  }
  renderSearchPanel() {
    return (
      <>
        <label>
            Search By Hashtag:
            <input type="text" value={this.state.hashTagSearchStr} onChange={this.filterDataByhashTag} />
        </label>
        <label>
            Filter By Location:
            <input type="text" value={this.state.filterByLocationStr} onChange={this.filterDataByLOcation} />
        </label>
      </>
    )
  }
  render() {
    const {name, email } = this.props.userData;
    const {tweets} = this.state;
    return (
      <div className="TweetContainer">
        <div className="AccountContainer">
          <div className="AccountContent">{name}</div>
          <div className="AccountContent">{email}</div>
        </div>
        <div className="SearchContainer">
          {this.renderSearchPanel()}
        </div>
        {tweets.map((data: any) => <TweetCard data={data}/>)}
      </div>
 );
  }
}

export default TweetScreen;