import React from 'react';
import {TweetType} from '../../App';
import './Tweet.screen.css';
import ReactHtmlParser from 'react-html-parser';


export interface Props {
    data: TweetType
}

const TwitterCard = (props: Props) => {
    const {created_at, text, source, user, entities} = props.data;
    const {hashtags, user_mentions} = entities;

    return (
        <div className="TweetCardContainer">
            <div className="CardContent">{created_at}</div>
            <div className="CardContent">{text}</div>
            <div className="CardContent">{ReactHtmlParser(source)}</div>
            HashTag: {hashtags.toString()} <br />
            Written By: {user.screen_name} <br />
            Location: {user.location} <br />
            Shared By: {user_mentions.map((user) => user.screen_name)}
        </div>
    )
};
export default React.memo(TwitterCard);