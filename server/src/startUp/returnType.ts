interface ReturnType {
  status: boolean;
  message: string;
  error?: any;
}
export default ReturnType;
