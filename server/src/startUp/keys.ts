import dotenv from 'dotenv';
import ReturnType from './returnType';
import fs from 'fs';

const BASE_PATH = '../keys/';

const setEnvironmentKeys = (): ReturnType => {
  const envType: string = process.env.NODE_ENV || '';
  if (process.env.IS_COMPILED_THROUGH_DOCKER) {
    return {
      status: true,
      message: `Environment:${envType} is set by docker`
    };
  }
  if (envType === 'prod') {
    return {
      status: true,
      message: `Environment:${envType} must be set by host env variables`
    };
  }
  const path: string = `${BASE_PATH}.${envType}.env`;
  if (!fs.existsSync(path)) {
    return {
      status: false,
      message: `${path}? ENV file not foun`
    };
  }
  try {
    dotenv.config({ path });
    return {
      status: true,
      message: `Environment:${envType} is set `
    };
  } catch (error) {
    return {
      status: false,
      message: `Environment:${envType} not set properly`,
      error: error
    };
  }
};
export default setEnvironmentKeys;
