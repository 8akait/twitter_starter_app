import DB from '../services/database';
import CreateDBcollectionAndValidation from '../models';

// boot up stages
async function* setupListGenerator() {
  yield await DB.init();
  yield await CreateDBcollectionAndValidation();
}

// iterate the setup
const bootstrapSetup = async (): Promise<any> => {
  try {
    const stage = setupListGenerator();
    let continues: boolean = true;
    do {
      const { done, value } = await stage.next();
      if (done === true) continues = false;
      else {
        if (!value) {
          console.log('void returned');
          return Promise.resolve(true);
        }
        const { error, status, message } = value;
        console.log(`
          message: ${message},
          status: ${status ? 'success' : 'failed'}
          ${error ? `error: ${error}` : ''}
          `);
      }
    } while (continues);
    return Promise.resolve(true);
  } catch (error) {
    return Promise.resolve(true);
    // return Promise.reject(false);
  }
};
export default bootstrapSetup;
