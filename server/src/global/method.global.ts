const isDefined = (value: any) => {
  return value !== null && value !== undefined;
};
export { isDefined };
