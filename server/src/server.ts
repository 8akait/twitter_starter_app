import App from './app';
import http from 'http';

function normalizePort(val: string): string | number | boolean {
  const port = parseInt(val, 10);
  if (isNaN(port)) {
    // named pipe
    return val;
  }
  if (port >= 0) {
    // port number
    return port;
  }
  return false;
}
function onError(error: any, port: string | number | boolean): void {
  if (error.syscall !== 'listen') {
    throw error;
  }
  const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}
function onListening(port: string | number | boolean): void {
  const addr = server.address();
  const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + port;
  console.log('Listening on ' + bind);
}

// Start Express server

const app = new App();
const server = http.createServer(app.app);

app.on(app.IS_SERVER_READY_EVENT, () => {
  const port = normalizePort(process.env.SERVER_PORT || '4000');
  server.listen(port);
  // get passport settings
  server.on('error', error => onError(error, port));
  server.on('listening', () => onListening(port));
});

export default server;
