import express, { Request, Response, NextFunction } from 'express';
import passport from 'passport';
import dummyData from '../dummyData/twitter.data.json';
import { UpdateUserTweet } from '../models/users/user.query';
import { AuthRequest } from '../global/interface.global';

const router = express.Router();

// twitter
router.get('/twitter', passport.authenticate('twitter'));
router.get(
  '/twitter/callback',
  passport.authenticate('twitter', { failureRedirect: '/auth/twitter' }),
  (req, res, next) => {
    res.status(201).send(req.user); // for website
  }
);

router.get(
  '/twitter/dummy',
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const response = await UpdateUserTweet({
        token: 'token_xyz',
        tokenSecret: 'tokenSecret_xyz',
        profile: {
          id: 'my_id12',
          name: 'my_name',
          email: 'my_email',
          twitterProfileData: dummyData
        }
      });
      return res.status(201).send(response);
    } catch (error) {
      return res.status(400).send({ error });
    }
  }
);

// google
router.get(
  '/google',
  passport.authenticate('google', {
    scope: ['profile', 'email']
  })
);
router.get(
  '/google/callback',
  passport.authenticate('google', { failureRedirect: '/auth/google' }),
  async (req: AuthRequest, res: Response, next: NextFunction) => {
    try {
      if (req && req.user) {
        const { id, name, emails } = req.user;
        const username = name.givenName;
        const email = emails[0].value;
        const twitterData: any[] = [];

        dummyData.statuses.map(tweet => {
          const { created_at, id_str, text, entities, user } = tweet;
          const { id_str: uid, screen_name, location } = user;
          const { hashtags, user_mentions } = entities;
          twitterData.push({
            created_at,
            id_str,
            text,
            entities: { hashtags, user_mentions },
            user: { id_str: uid, screen_name, location }
          });
        });

        const response = await UpdateUserTweet({
          name: username,
          email: email,
          twitter: {
            token: 'token_xyz',
            tokenSecret: 'tokenSecret_xyz',
            id: id,
            tweets: twitterData
          }
        });

        const result = Object.assign(
          { name: username, email },
          {
            twitter: { tweets: twitterData }
          }
        );
        const str = JSON.stringify(result);
        return res.redirect('http://localhost:3000/auth/redirect?user=' + str);
      }
      return res.redirect('http://localhost:3000/');
    } catch (error) {
      console.log(error);
      return res.status(400).send({ error });
    }
  }
);

export default router;
