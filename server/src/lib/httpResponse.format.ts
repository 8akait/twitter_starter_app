import { Response } from 'express';
type NextStates = 'logout' | 'refreshJwt' | undefined;

type ServiceProvider = 'server';
interface ExtensionType {
  context: any;
  runtime: number;
  analytics: any[];
  [val: string]: any;
}
interface InputData {
  status: 'success' | 'failed';
  data?: any;
  statusCode?: number;
  next?: NextStates;
  message: string;
  extensions?: ExtensionType | undefined;
  res: Response;

  errors?: any;
  isValidationError?: boolean;
}
class HttpResponse {
  private status: 'success' | 'failed';
  private data: any;
  private statusCode: number;
  private next: NextStates;
  private message: string = '';
  private extensions: ExtensionType | undefined;

  private res: Response;

  private errors: any;

  private errorType: 'normal' | 'validation' | undefined;

  private serviceProvider: ServiceProvider;

  constructor({
    status,
    data,
    statusCode,
    next,
    message,
    extensions,
    res,
    errors,
    isValidationError
  }: InputData) {
    this.serviceProvider = 'server';
    if (status === 'failed') {
      statusCode = statusCode ? statusCode : 400;
      this.errors = errors;
    } else {
      this.data = data;
    }
    if (isValidationError) {
      statusCode = statusCode ? statusCode : 422;
      this.errorType = 'validation';
    }

    this.status = status;
    this.statusCode = statusCode || 200;
    this.next = next;
    this.message = message;
    this.extensions = extensions;
    this.res = res;
    this.send();
  }
  send() {
    return this.res.status(this.statusCode).send({
      errorType: this.errorType,
      errors: this.errors,
      data: this.data,
      status: this.status,
      next: this.next,
      message: this.message,
      serviceProvider: this.serviceProvider
    });
  }
}

export default HttpResponse;
