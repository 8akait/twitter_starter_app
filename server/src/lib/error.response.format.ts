type ServiceProvider = 'server';

interface ExtensionsType {
  gitSHA?: string | undefined;

  userId?: string | undefined;

  requestParams?: any;

  nodeVersion: string;
  context?: any | undefined;

  route: string | undefined;
  serviceProvider: ServiceProvider | undefined;
}
interface ErrorType {
  message: string;
  sourceType?: 'rest';

  target?: 'query' | 'mutation';

  errorMessage: string | undefined;

  stack: any;
  extensions?: ExtensionsType;
  locations?: any;
}
interface ErrorArgType {
  statusCode: number | undefined;
  userData: any;
  route: string | undefined;
  requestParams: any;
  message: string;
  stack: any;
  sourceType: 'rest' | undefined;
  target: 'query' | 'mutation' | undefined;
  errorMessage: string | undefined;
  context: any;
  locations: [] | undefined;
  serviceProvider: ServiceProvider;
}

class ErrorResponse {
  private timestamp: Date = new Date();
  private status: string;

  private statusCode: number;

  private errors: ErrorType;

  constructor({
    statusCode,
    userData,
    route,
    requestParams,
    message,
    stack,
    sourceType,
    target,
    errorMessage,
    context,
    locations,
    serviceProvider
  }: ErrorArgType) {
    this.status = 'error';
    this.statusCode = statusCode || 500;

    const nodeVersion = process.version;
    const gitSHA = process.env.gitSHA;

    const extensions: ExtensionsType = {
      gitSHA,
      userId: userData ? userData._id : undefined,
      requestParams,
      nodeVersion,
      route,
      context,
      serviceProvider
    };
    this.errors = {
      message,
      sourceType,
      target,
      errorMessage,
      stack: stack && stack.split ? stack.split('\n') : [],
      extensions,
      locations
    };
  }
}
export default ErrorResponse;
