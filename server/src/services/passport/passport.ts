import passport from 'passport';
import TwitterStrat from 'passport-twitter';
import GoogleStrat from 'passport-google-oauth20';

const TwitterStrategy = TwitterStrat.Strategy;
const GoogleStrategy = GoogleStrat.Strategy;

const {
  TWITTER_CLIENT_ID,
  TWITTER_CLIENT_SECRET,
  TWITTER_CALLBACK_URL,
  GOOGLE_CLIENT_ID,
  GOOGLE_CLIENT_SECRET,
  GOOGLE_CALLBACK_URL
} = process.env;

// serialize user data
passport.serializeUser((user: any, done) => {
  done(undefined, user);
});

passport.use(
  new TwitterStrategy(
    {
      consumerKey: TWITTER_CLIENT_ID || 'sd',
      consumerSecret: TWITTER_CLIENT_SECRET || 'sd',
      callbackURL: TWITTER_CALLBACK_URL || 'sd'
    },
    function(token, tokenSecret, profile, cb) {
      //   User.findOrCreate({ twitterId: profile.id }, function(err: any, user: any) {
      //     return cb(err, user);
      //   });
      return cb(undefined, profile);
    }
  )
);

// google auth
passport.use(
  new GoogleStrategy(
    {
      clientID: GOOGLE_CLIENT_ID || 'sd',
      clientSecret: GOOGLE_CLIENT_SECRET || 'sd',
      callbackURL: GOOGLE_CALLBACK_URL || 's'
    },
    (accessToken, refreshToken, profile, done) => {
      return done(undefined, profile);
    }
  )
);
