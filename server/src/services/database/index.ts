import ReturnType from '../../startUp/returnType';
import { MongoDbUSER, MongoDbLOCATION, MongoDbLOGS } from './mongo.db';
class DB {
  private static mongoUserdb: any;
  private static mongoLocationdb: any;
  private static mongoLogsdb: any;
  private static redisUserdb: any;
  private static elasticSearchUserdb: any;
  private constructor() {}
  static async init(): Promise<ReturnType> {
    let msg = '';
    try {
      if (!DB.mongoUserdb) {
        const MongoDbUSERStatus = await MongoDbUSER.init();
        DB.mongoUserdb = MongoDbUSERStatus.instance;
        msg = msg + ` ${MongoDbUSERStatus.dbNmae}:${MongoDbUSERStatus.status} `;
      }
      return Promise.resolve({
        status: true,
        message: msg
      });
    } catch (err) {
      console.log(err);
      const { status, message, error } = err;
      return Promise.reject({
        status,
        message,
        error
      });
    }
  }
  static get mongoUser(): any {
    return DB.mongoUserdb;
  }
  static closeMongoUser(): void {
    MongoDbUSER.close();
  }
}
export default DB;
