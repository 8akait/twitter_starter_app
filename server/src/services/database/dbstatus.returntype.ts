interface DBStatusReturnType {
  status: string;
  dbNmae: string;
  instance?: string;
  error?: any;
}
export default DBStatusReturnType;
