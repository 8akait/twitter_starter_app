import mongodb from 'mongodb';
import DBStatusReturnType from './dbstatus.returntype';
class MongoDbUSER {
  private mongoURL: string = process.env.MONGO_USER_URL || '';
  private mongoDatabaseName: string = process.env.MONGO_USER_DATABASE || '';
  private MongoClient: any;

  private static client: any;
  private static instance: any;

  private constructor() {
    // console.log(`MongoUrl: ${process.env.MONGO_USER_URL}`);
    // console.log(`MongoDB: ${process.env.MONGO_USER_DATABASE}`);
    this.MongoClient = new mongodb.MongoClient(this.mongoURL, {
      useNewUrlParser: true
    });
  }

  static async close() {
    await MongoDbUSER.client.close();
  }

  static async init(): Promise<DBStatusReturnType> {
    try {
      if (!MongoDbUSER.instance) {
        const instance = new MongoDbUSER();
        MongoDbUSER.client = await instance.MongoClient.connect();
        MongoDbUSER.instance = MongoDbUSER.client.db(
          instance.mongoDatabaseName
        );
      }
      return Promise.resolve({
        status: 'ready',
        dbNmae: 'mongo_user',
        instance: MongoDbUSER.instance
      });
    } catch (err) {
      return Promise.reject({
        status: 'failed',
        dbNmae: 'mongo_user',
        error: err
      });
    }
  }
}
class MongoDbLOCATION {
  private mongoURL: string = process.env.MONGO_LOCATION_URL || '';
  private mongoDatabaseName: string = process.env.MONGO_LOCATION_DATABASE || '';
  private MongoClient: any;

  private static client: any;
  private static instance: any;

  private constructor() {
    // console.log(`MongoUrl: ${process.env.MONGO_USER_URL}`);
    // console.log(`MongoDB: ${process.env.MONGO_USER_DATABASE}`);
    this.MongoClient = new mongodb.MongoClient(this.mongoURL, {
      useNewUrlParser: true
    });
  }

  static async close() {
    await MongoDbLOCATION.client.close();
  }

  static async init(): Promise<DBStatusReturnType> {
    try {
      if (!MongoDbLOCATION.instance) {
        const instance = new MongoDbLOCATION();
        MongoDbLOCATION.client = await instance.MongoClient.connect();
        MongoDbLOCATION.instance = MongoDbLOCATION.client.db(
          instance.mongoDatabaseName
        );
      }
      return Promise.resolve({
        status: 'ready',
        dbNmae: 'mongo_location',
        instance: MongoDbLOCATION.instance
      });
    } catch (err) {
      return Promise.reject({
        status: 'failed',
        dbNmae: 'mongo_location',
        error: err
      });
    }
  }
}
class MongoDbLOGS {
  private mongoURL: string = process.env.MONGO_LOGS_URL || '';
  private mongoDatabaseName: string = process.env.MONGO_LOGS_DATABASE || '';
  private MongoClient: any;

  private static client: any;
  private static instance: any;

  private constructor() {
    // console.log(`MongoUrl: ${process.env.MONGO_LOGS_URL}`);
    // console.log(`MongoDB: ${process.env.MONGO_LOGS_DATABASE}`);
    this.MongoClient = new mongodb.MongoClient(this.mongoURL, {
      useNewUrlParser: true
    });
  }

  static async close() {
    await MongoDbLOGS.client.close();
  }

  static async init(): Promise<DBStatusReturnType> {
    try {
      if (!MongoDbLOGS.instance) {
        const instance = new MongoDbLOGS();
        MongoDbLOGS.client = await instance.MongoClient.connect();
        MongoDbLOGS.instance = MongoDbLOGS.client.db(
          instance.mongoDatabaseName
        );
      }
      return Promise.resolve({
        status: 'ready',
        dbNmae: 'mongo_logs',
        instance: MongoDbLOGS.instance
      });
    } catch (err) {
      return Promise.reject({
        status: 'failed',
        dbNmae: 'mongo_logs',
        error: err
      });
    }
  }
}
export { MongoDbUSER, MongoDbLOCATION, MongoDbLOGS };
