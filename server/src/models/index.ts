import createUserCollection from './users/user.model';

const CreateDBcollectionAndValidation = async () => {
  try {
    const userStatus = await createUserCollection();
    const allStatus = ['created collections: ', userStatus];
    const message = allStatus.join(', ');
    return {
      status: 'done',
      message: message
    };
  } catch (error) {
    return {
      error,
      status: 'no',
      message: 'failed'
    };
  }
};

export default CreateDBcollectionAndValidation;
