import DB from '../../services/database';
const schema = {
  $jsonSchema: {
    bsonType: 'object',
    required: ['name', 'email'],
    properties: {
      name: {
        bsonType: 'string',
        minLength: 3,
        maxLength: 20
      },
      email: {
        bsonType: 'string'
      },
      twitter: {
        bsonType: 'object',
        additionalProperties: false,
        properties: {
          id: {
            bsonType: 'string'
          },
          token: {
            bsonType: 'string'
          },
          tokenSecret: {
            bsonType: 'string'
          },
          tweets: {
            bsonType: 'array',
            items: {
              bsonType: 'object',
              additionalProperties: false,
              properties: {
                created_at: {
                  bsonType: 'string'
                },
                id_str: {
                  bsonType: 'string'
                },
                text: {
                  bsonType: 'string'
                },
                source: {
                  bsonType: 'string'
                },
                user: {
                  bsonType: 'object',
                  additionalProperties: false,
                  properties: {
                    id_str: {
                      bsonType: 'string'
                    },
                    screen_name: {
                      bsonType: 'string'
                    },
                    location: {
                      bsonType: 'string'
                    }
                  }
                },
                entities: {
                  bsonType: 'object',
                  additionalProperties: false,
                  properties: {
                    hashtags: {
                      bsonType: 'array',
                      items: {
                        bsonType: 'string'
                      }
                    },
                    user_mentions: {
                      bsonType: 'array',
                      items: {
                        bsonType: 'object',
                        additionalProperties: false,
                        properties: {
                          id_str: {
                            bsonType: 'string'
                          },
                          screen_name: {
                            bsonType: 'string'
                          },
                          location: {
                            bsonType: 'string'
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      },
      createdOn: {
        bsonType: 'date'
      },
      deletedOn: {
        bsonType: 'date'
      },
      updatedOn: {
        bsonType: 'date'
      }
    }
  }
};
const createUserCollection = async (): Promise<string> => {
  try {
    const db = DB.mongoUser;
    await db.createCollection('users');
    await db.command({
      collMod: 'users',
      validator: schema,
      validationLevel: 'moderate',
      validationAction: 'warn'
    });
    await db.collection('users').createIndex({ email: 1 }, { unique: true });
    return Promise.resolve('users');
  } catch (error) {
    throw new Error(error);
  }
};
export default createUserCollection;
