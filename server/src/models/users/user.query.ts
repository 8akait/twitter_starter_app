import DB from '../../services/database';
import mongo from 'mongodb';
import _ from 'lodash';
import { isDefined } from '../../global/method.global';

interface CreateUserInterface {
  name: string;
  email: string;
  twitter?: TwitterDataType;
}
const createUser = async (data: CreateUserInterface): Promise<any> => {
  const { name, email, twitter } = data;
  const dbData = _.pickBy(
    {
      name,
      email,
      twitter
    },
    isDefined
  );
  try {
    const result = await DB.mongoUser.collection('users').insertOne(dbData);
    return Promise.resolve({
      data: result
    });
  } catch (error) {
    return Promise.reject(error);
  }
};

const findUserByTwitterId = async (
  emailId: string,
  twitterId: string
): Promise<any> => {
  try {
    const userData = await DB.mongoUser
      .collection('users')
      .findOne({ $or: [{ 'twitter.id': twitterId }, { email: emailId }] });
    return Promise.resolve(userData);
  } catch (error) {
    return Promise.reject(error);
  }
};

interface TwitterUserType {
  id: string;
  screen_name: string;
  location?: string;
}

interface EntitiesType {
  hashtags: string[];
  user_mentions: TwitterUserType[];
}
interface TweetType {
  created_at: string;
  id: string;
  text: string;
  source: string;
  user: TwitterUserType;
  entities: EntitiesType;
}

interface TwitterDataType {
  id: string;
  token: string;
  tokenSecret: string;
  tweets: TweetType[];
}

const UpdateUserTweet = async ({ name, email, twitter }: any): Promise<any> => {
  try {
    const { id, tweets, token, tokenSecret } = twitter;
    let userData = await findUserByTwitterId(email, id);

    const twitterData: TwitterDataType = {
      id,
      token,
      tokenSecret,
      tweets: tweets
    };

    if (!userData) {
      userData = await createUser({ name, email, twitter: twitterData });
      return Promise.resolve(userData.data.ops[0]);
    } else {
      const { _id } = userData;
      const response = await DB.mongoUser.collection('users').findOneAndUpdate(
        {
          _id
        },
        { $set: { twitter: twitterData } },
        { returnOriginal: false }
      );
      return Promise.resolve(response.value);
    }
  } catch (error) {
    console.log(error);
    return Promise.reject(error);
  }
};

export { UpdateUserTweet };
