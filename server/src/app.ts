import express, { Request, Response, NextFunction } from 'express';
import compression from 'compression'; // compresses requests
import bodyParser from 'body-parser';
import lusca from 'lusca';
import helmet from 'helmet';
import cors from 'cors';
import expressValidator from 'express-validator';
import errorHandler from 'errorhandler';
import events from 'events';
import passport from 'passport';

import ErrorResponse from './lib/error.response.format';
import bootstrapSetup from './startUp';

import setEnvironmentKeys from './startUp/keys';

// Controllers (route handlers)

import { AuthRequest } from './global/interface.global';

// routes
import authRoutes from './routes/auth.route';

// set all keys
const output = setEnvironmentKeys();
console.log(output);

// get passport setting
import './services/passport/passport';

class App extends events {
  public app: express.Application;
  public IS_SERVER_READY_EVENT: string = 'setupReady';
  private env: string = process.env.NODE_ENV || 'dev';
  constructor() {
    super();
    this.app = express();
    this.bootstrapSetup();
    this.initializeMiddlewares();
    this.env === 'dev' || this.env === 'local'
      ? this.initializeDevMiddleware()
      : undefined;
    this.initializeRoutes();
    this.errorHandler();
  }
  private async bootstrapSetup(): Promise<any> {
    const isReady = await bootstrapSetup();
    this.emit(this.IS_SERVER_READY_EVENT);
  }

  private initializeMiddlewares() {
    this.app.use(compression());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(expressValidator());
    this.app.use(lusca.xframe('SAMEORIGIN'));
    this.app.use(lusca.xssProtection(true));
    this.app.use(helmet());
    this.app.use(cors());
    this.app.use(function(req: Request, res: Response, next: NextFunction) {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
      );
      res.setHeader(
        'Access-Control-Allow-Methods',
        'POST, GET, PATCH, DELETE, OPTIONS'
      );
      next();
    });
    this.app.use(passport.initialize());
  }
  private initializeDevMiddleware(): void {
    if (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'local') {
      this.app.use(errorHandler());
    }
    this.app.use((req: Request, res: Response, next: NextFunction) => {
      console.log(req.body);
      console.log(req.headers);
      next();
    });
  }

  private initializeRoutes(): void {
    this.app.get('/', (req, res) => res.status(200).send({ name: 'arpit' }));

    // authentication
    this.app.use('/auth', authRoutes);
  }
  private errorHandler(): void {
    // catch 404 and forward to error handler
    this.app.use(
      (error: any, req: AuthRequest, res: Response, next: NextFunction) => {
        if (error) {
          const { body, params, query, route, user, statusCode } = req;
          const {
            message = '',
            stack = '',
            locations = '',
            errorMessage
          } = error;

          const errorResponse = new ErrorResponse({
            statusCode,
            userData: user,
            route: route && route.path ? route.path : undefined,
            requestParams: { ...body, ...params, ...query },
            message: 'Something went wrong',
            stack,
            sourceType: 'rest',
            target: undefined,
            errorMessage: errorMessage ? errorMessage : message,
            context: undefined,
            locations,
            serviceProvider: 'server'
          });
          return res.status(500).send(errorResponse);
          // errorResponse.broadcast();
        } else {
          const { body, params, query, route } = req;
          const errorResponse = new ErrorResponse({
            statusCode: req.statusCode,
            userData: undefined,
            route: route,
            requestParams: { ...body, ...params, ...query },
            message: 'Page not found',
            stack: undefined,
            sourceType: 'rest',
            target: undefined,
            errorMessage: 'Page not found',
            context: undefined,
            locations: undefined,
            serviceProvider: 'server'
          });
          return res.status(400).send(errorResponse);
        }
      }
    );
  }
}
export default App;
